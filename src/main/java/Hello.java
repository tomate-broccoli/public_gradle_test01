// Hello.java
public class Hello {

    String name = "Gitlab";
    
    public static void main(String[] args){
        new Hello().run();
    }

    void run(){
        System.out.printf("Hello, %s!\n", getName());
    }

    String getName(){
        return name;
    }
}
